# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
from __future__ import division

import sys
reload(sys)
sys.setdefaultencoding('utf8')

from psychopy import core, visual, event, data, gui, sound
import csv, random, time, datetime


#### PREPARATION DE L'EXPERIENCE ####

#GUI de recueil de données relatives au participant

exp_info = {'Sujet':'', 'Age':'', 'Sexe':['F','H'],'Lateralite':['D','G'],'Verbalisation':['O','N'],'Filtrage':['HFS','BFS','NF'], 'Experimentateur':''}
dlg = gui.DlgFromDict(dictionary = exp_info)
if dlg.OK == False:
   core.quit()

    ## Sujet = numéro du Sujet
    ## Age = age du sujet ; Sexe = sexe du sujet (homme ou femme)
    ## Lateralite = lateralite du sujet (droitier ou gaucher) pour parametrage des réponses clavier en tâche de reconnaissance
    ## VI 1 : Verbalisation = description ou absence de description
    ## VI 2 : Filtrage = filtres passes-bas, filtres passe-haut, absence de filtre sur les stimuli présentés au cours de la tâche de reconnaissance
    ## Experimentateur = nom de l'experimentateur 

#Préparation de l'affichage

win = visual.Window([1366,768], units='pix',allowGUI=True,monitor='testMonitor', fullscr=True)  
##fullscr=True
##Remarque : enlever l'argument fullscr pour un affichage en taille réduite qui permet l'arrêt prématuré de l'expérience

## Fond gris 128 :
win.colorSpace = 'rgb255' 
win.color = [128, 128, 128] 


#Chemin des fichiers

stim_path = './stims/' ##stimuli
listesStim_path = './listesStim/' ##classeurs contenant des informations sur les stimuli
data_path = './data/' ## stockage des fichiers individuels sujet + remplissage de la matrice générale


#Récupération des listes de stimuli depuis un classeur excel
    #Organisation aléatoire de leur présentation sur le bloc encodage et sur les blocs reconnaissance

    ##Bloc encodage
liste_cible = data.importConditions(listesStim_path + 'ListeCibles.xlsx')
cibleFile = data.TrialHandler(liste_cible,1,method = 'random')

    ##Blocs tâche de reconnaissance

        ### Stimuli Femmes
listeF02KDE = data.importConditions(listesStim_path + 'F02KDE.xlsx')
Groupe_F02_KDE = data.TrialHandler(listeF02KDE,1,method = 'random')

listeF003Chicago = data.importConditions(listesStim_path + 'F003Chicago.xlsx')
Groupe_F003_Chicago = data.TrialHandler(listeF003Chicago,1,method = 'random')

listeF091London = data.importConditions(listesStim_path + 'F091London.xlsx')
Groupe_F091_London = data.TrialHandler(listeF091London,1,method = 'random')

listeF107Chicago = data.importConditions(listesStim_path + 'F107Chicago.xlsx')
Groupe_F107_Chicago = data.TrialHandler(listeF107Chicago,1,method = 'random')

listeF248Chicago = data.importConditions(listesStim_path + 'F248Chicago.xlsx')
Groupe_F248_Chicago = data.TrialHandler(listeF248Chicago,1,method = 'random')


        ### Stimuli Hommes
listeH013KDE = data.importConditions(listesStim_path + 'H013KDE.xlsx')
Groupe_H013_KDE = data.TrialHandler(listeH013KDE,1,method = 'random')

listeH203Chicago = data.importConditions(listesStim_path + 'H203Chicago.xlsx')
Groupe_H203_Chicago = data.TrialHandler(listeH203Chicago,1,method = 'random')

listeH117London = data.importConditions(listesStim_path + 'H117London.xlsx')
Groupe_H117_London = data.TrialHandler(listeH117London,1,method = 'random')

listeH245Chicago = data.importConditions(listesStim_path + 'H245Chicago.xlsx')
Groupe_H245_Chicago = data.TrialHandler(listeH245Chicago,1,method = 'random')

listeH249Chicago = data.importConditions(listesStim_path + 'H249Chicago.xlsx')
Groupe_H249_Chicago = data.TrialHandler(listeH249Chicago,1,method = 'random')


#Récupération des données démographiques dans un classeur excel 

    ##Paramétrage du nom du fichier 
date = datetime.datetime.now() ###Récupération de la date de la passation
fileName ="SUJET"+exp_info.get("Sujet")+str(date.year)+str(date.month)+str(date.day) ### Nom du fichier = id du sujet + date de la passation

    ##Paramétrage du classeur 
datafile = open(data_path+fileName+".csv", "wb") ###Remarque : En fonction de la version de Python mettre l'argument "wb" OU "w"
writer = csv.writer(datafile, delimiter=";")
writer.writerow([exp_info.get("Sujet"),exp_info.get("Age"),exp_info.get("Sexe"),exp_info.get("Lateralite"),exp_info.get("Verbalisation"),exp_info.get("Filtrage"),exp_info.get("Experimentateur")])
writer.writerow(["ID", "sexe", "age", "verbalisation", "filtrage","bloc","essai","liste", "type","idStim", "repClavier","typeRep","BR", "TR", "PT_Med", "PT_Moyenne", "PT_Fidelite", "PT_Rang"])
 
    ##ID = identifiant du sujet, sexe= sexe du participation, age = âge du participant
    ##verbalisation = vi verbalisation, filtrage = vi filtrage
    ##bloc = numéro du bloc, essai = numéro de l’essai, liste= numéro du groupe auquel appartient le stimulus, type =distracteur ou cible 
    ##idStim = identifiant du stimulus 
    ##repClavier = réponse clavier du participant, typeRep= type de réponse rejet correct, hit, FA, omission 
    ##BR= justesse de la réponse, TR = temps de réaction
    ##PT_... = indices des pré-tests sur les stimuli 

#Gestion de l'ID du sujet et stockage des informations socio-D au format String 

Age=str(exp_info.get("Age"))
Sujet=str(exp_info.get("Sujet"))
Sexe = exp_info.get("Sexe")

if Sexe== 'F':
    Sexe="F"
elif Sexe=='H':
    Sexe="H"
else:
    print("erreur dans le paramétrage de l'expérience sur la variable Sexe")
    datafile.close()
    win.close()
    core.quit()


ID="S"+Sujet+"_"+Age+"_"+Sexe+"_"+str(date.year)+str(date.month)+str(date.day) ## ID = S + identifiant du sujet + Age + Sexe + date de la passation

#Récupération du nom de l'expérimentateur en vue d'un stockage ultérieur 
Experimentateur=str(exp_info.get("Experimentateur"))


#Gestion des VI

    ##VI Verbalisation

verbalisationCond=exp_info.get("Verbalisation")
if verbalisationCond=='O':
    verbalisationCondF="Description"
elif verbalisationCond=='N':
    verbalisationCondF="Absence de Description"
else:
    print("erreur dans le paramétrage de l'expérience sur la condition de description")
    datafile.close()
    win.close()
    core.quit()
    
    ##VI Filtrage 
    ## Remarque : le chemin d'accès aux images filtrées pour la tâche de reconnaissance est géré selon les conditions définies dans la partie ci-dessous 

filtrageCond=exp_info.get("Filtrage")
if filtrageCond=='NF':
    filtrageCondF="Non filtré"
    pathCond="NF/"
elif filtrageCond=='HFS':
    filtrageCondF="Filtrage passe haut"
    pathCond="HFS/"
elif filtrageCond=='BFS':
    filtrageCondF="Filtrage passe bas"
    pathCond="BFS/"
else : 
    print("erreur dans le paramétrage de l'expérience sur la VI filtrage")
    datafile.close()
    win.close()
    core.quit()


#Gestion de la latéralité 
 
    ##la réponse “oui” est présentée sur la main dominante du participant
    ##la consigne est donc ajustée en fonction de la latéralisation du participant
    ## A MODIFIER EN FONCTION DE LA COULEUR DE LA GOMMETTE 

lateralite = exp_info.get("Lateralite")
if lateralite == 'D':
    oui = "m"
    non = "q"
    Textconsigne = "Nous allons vous présenter une série de visages. Dans chaque essai, appuyez sur la touche du clavier \"bleue\", lorsque vous pensez avoir déjà vu le visage présenté. Sinon appuyez sur la touche \"rouge\"."
elif lateralite == 'G':
    oui = "q"
    non = "m"
    Textconsigne = "Nous allons vous présenter une série de visages. Dans chaque essai, appuyez sur la touche du clavier \"rouge\", lorsque vous pensez avoir déjà vu le visage présenté. Sinon appuyez sur \"bleue\"."
else : 
    print("erreur dans le paramétrage de l'expérience sur la latéralité")
    datafile.close()
    win.close()
    core.quit()


#### PASSAGE DE L'EXPERIENCE ####

# TACHE D ENCODAGE

    ##Consignes phase d'encodage 

consigne = visual.TextStim(win,
                           text= 'Vous allez voir apparaître à l\'écran une série de visages présentés sucessivement, regardez-les attentivement', 
                           color="white",
                           height=24)
consigne.draw()
win.flip()
event.waitKeys() 
event.clearEvents()

consigne = visual.TextStim(win,
                           text= 'Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la visualisation', 
                           color="white",
                           height=24)
consigne.draw()
win.flip()
event.waitKeys() 
event.clearEvents() #réinitialisation des événements clavier 

    ##Paramètres de la boucle : 10 visages cible + 1 visage à décrire / ou non 

numCible=0 
ordreCible=[1,2,3,4,5,6,7,8,9,10]
i=0

    ## Affichage des cibles à encoder 
    
for numCible in cibleFile : 
    
    if i==0:
        #Croix de fixation
        fix=visual.TextStim(win, alignHoriz='center',color="white", height=40)
        fix.setText("+") 
        fix.draw()
        win.flip()
        core.wait(2)
    else :
        emptyScreen = visual.ImageStim(win, image =stim_path+"EmptyScreen.jpg")
        emptyScreen.draw()
        win.flip()
        core.wait(0.5)
    
    #Affichage de la cible
    ciblePath=numCible['Path'] ## Récupération du chemin vers l'image 
    cibleGroupe=numCible['IDGroupe'] ## Récupération de l'identifiant de la cible 
    cible = visual.ImageStim(win, image =stim_path+"NF/"+ciblePath)
    cible.draw()
    win.flip()
    core.wait(5) ## 5 secondes de présentation
    
    #Récupération de l'ordre de présentation des cibles pour paramétrer l'ordre des blocs sur la tâche de reconnaissance
    ordreCible[i]=cibleGroupe
    i=i+1 ##i constitue l'indice qui gère la liste ordreCible
    ## Remarque : numCible est définie avec la fonction TrialHandler comme une variable dictionnaire et ne peut donc pas faire boucler la liste
    
print(ordreCible) #Impression console de l'ordre d'apparition des cibles pour vérification 

    ## Affichage de la cible indépendante à décrire en condition description

emptyScreen = visual.ImageStim(win, image =stim_path+"EmptyScreen.jpg")
emptyScreen.draw()
win.flip()
core.wait(0.5)

cible = visual.ImageStim(win, image =stim_path+"CibleADecrire_CFD-WM-251-002-N.jpg")
cible.draw()
win.flip()
core.wait(5)


#TACHE DISTRACTRICE + DESCRIPTION 

finVisualisation=visual.TextStim(win, alignHoriz='center',color="white",height=30)
finVisualisation.setText("Fin de la présentation des visages") 
finVisualisation.draw()
win.flip()
core.wait(3)

    
if verbalisationCond=='O':
    
    ## Bip de timing pour l'expérimentateur
    highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
    highA.setVolume(0.8)
    highA.play()
    core.wait(0.8)
    
    #TACHE DISTRACTIVE 
    
        ##Consigne de passage la tâche distractrice
    
    distract=visual.TextStim(win, alignHoriz='center',color="white",height=30)
    distract.setText("Suivez les instructions de l'expérimentateur pour la tâche qui va suivre") 
    distract.draw()
    win.flip()
    event.waitKeys()
    
    event.clearEvents()
    
    consigne = visual.TextStim(win,
                           text= 'Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la tâche', 
                           color="white", 
                           alignHoriz='center',
                           alignVert='center',
                           height=24)
    consigne.draw()
    win.flip()
    event.waitKeys() 
    
    distractD=visual.TextStim(win, alignHoriz='center',color="white",height=40)
    distractD.setText(" Durée : 10 minutes") 
    distractD.draw()
    win.flip()
    #core.wait(5)
    core.wait(600) #10 minutes de tâche distractrice soit 600 secondes

        ## Bip de timing pour l'expérimentateur 
    highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
    highA.setVolume(0.8)
    highA.play()

    
    event.clearEvents() #réinitialisation des événements clavier
    
    
    #DESCRIPTION 
    
    descript=visual.TextStim(win, alignHoriz='center',color="white",height=22)
    descript.setText("Vous allez maintenant essayer de décrire le dernier visage qui vous a été présenté. Pour cela, essayez d’être le plus précis possible afin qu’une autre personne soit capable - par simple lecture de votre description - de se construire une représentation fidèle de ce visage.") 
    descript.draw()
    win.flip()
    event.waitKeys()
    
    event.clearEvents()
    
    consigne = visual.TextStim(win,
                           text= 'Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la description', 
                           color="white", 
                           alignHoriz='center',
                           alignVert='center',
                           height=24)
    consigne.draw()
    win.flip()
    event.waitKeys() 
    
    descriptD=visual.TextStim(win, alignHoriz='center',color="white",height=40)
    descriptD.setText("Durée : 5 minutes") 
    descriptD.draw()
    win.flip()
    #core.wait(5)
    core.wait(300) #5 minutes soit 300 secondes
    
    event.clearEvents() #réinitialisation des événements clavier

    
elif verbalisationCond=='N':
    
    ## Bip de timing pour l'expérimentateur
    highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
    highA.setVolume(0.8)
    highA.play()
    core.wait(0.8)
    
    #TACHE DISTRACTRICE 
    
        ##Consigne de passage la tâche distractrice
        
    distract=visual.TextStim(win, alignHoriz='center',color="white", height=30)
    distract.setText("Suivez les instructions de l'expérimentateur pour la tâche qui va suivre") 
    distract.draw()
    win.flip()
    event.waitKeys()
    
    event.clearEvents()
    
    consigne = visual.TextStim(win,
                           text= 'Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la tâche', 
                           color="white",
                           height=24)
    consigne.draw()
    win.flip()
    event.waitKeys()
    
    distract=visual.TextStim(win, alignHoriz='center',color="white", height=40)
    distract.setText("Durée : 15 minutes") 
    distract.draw()
    win.flip()
    #core.wait(5)
    core.wait(900) #15 minutes de tâche distractrice soit 900 secondes
    event.clearEvents() #réinitialisation des événements clavier


## Bip de timing pour l'expérimentateur 
highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
highA.setVolume(0.8)
highA.play()
core.wait(0.8)

event.clearEvents() #réinitialisation des événements clavier

distract=visual.TextStim(win, alignHoriz='center',color="white",height=30)
distract.setText("Appuyez sur la barre d\'espace pour poursuivre l'expérience") 
distract.draw()
win.flip()
event.waitKeys()
event.clearEvents() #réinitialisation des événements clavier


#TACHE DE RECONNAISSANCE 

##Affichage de la consigne réponse clavier paramétrée en amont en fonction de la latéralisation du participant

consigne = visual.TextStim(win,color="white",height=20)
consigne.setText(Textconsigne) #se référer au paramétrage de l'expérience pour le texte de cette consigne
consigne.draw()
win.flip()
event.waitKeys()

event.clearEvents()

if filtrageCond=='HFS' or filtrageCond=='BFS':
    consigneFiltre = visual.TextStim(win,color="white",height=30)
    consigneFiltre.setText("Attention, les visages présentés seront modifiés !")
    consigneFiltre.draw()
    win.flip()
    core.wait(5)

consigne = visual.TextStim(win,
                           text= 'Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la tâche', 
                           color="white", 
                           alignHoriz='center',
                           alignVert='center',
                           height=24)
consigne.draw()
win.flip()
event.waitKeys()
event.clearEvents() #réinitialisation des événements clavier

##Paramètres de la tâche de reconnaissance : numéro du bloc, numéro de l'essai et compteur de bonnes réponses
NumBloc = 0
NumEssai= 1 
comptBR=0
comptHit=0
comptRejet=0
comptO=0
comptFR=0
comptTR=0


##Début de la tâche de reconnaissance

while NumBloc < len(ordreCible) :
    
    #Début du bloc 
    IDbloc=NumBloc+1
    debBloc = visual.TextStim(win,color="white",height=40)
    debBloc.setText("Début du bloc "+str(IDbloc)) 
    debBloc.draw()
    win.flip()
    core.wait(1)
    
    #Définition de la liste courante en fonction de l'ordre de présentation des cibles en encodage
    listeCourante=eval(ordreCible[NumBloc])
    
    #Croix de fixation
    fix=visual.TextStim(win, alignHoriz='center',color="white", height=40)
    fix.setText("+") 
    fix.draw()
    win.flip()
    core.wait(2)
    
    for numStim in listeCourante:
        
        #Affichage de la cible
        ciblePath=numStim['Path'] ## Récupération du chemin du stimulus 
        cible = visual.ImageStim(win, image =stim_path+pathCond+ciblePath) ##se référer au paramétrage de l'expérience pour la valeur de pathCond
        cible.draw()
        win.flip()
        
        imageTime = time.time()         ##variable temps associée à l'image
        key = event.waitKeys(1000,['q','m', 'escape'])  ## temps maximal fixé à 1000 sec, sinon erreur mémoire vive
        responseTime = time.time()       ##variable temps de réponse
        
        #Gestion du type du stimulus
        stimType=numStim['Type']
        
        #Traitement de la réponse clavier
        
        if key[0] == oui and stimType == "Cible" :       #Hit correct
            BR = 1
            R_Type="Hit"
            Hit=1
            
            Rejet=0
            O=0
            FR=0
            
        elif key[0] == non and stimType == "Distracteur" :      #Rejet correct
            BR = 1
            R_Type="Rejet"
            Rejet=1
            
            Hit=0
            FR=0
            O=0
            
        elif key[0]== non and stimType == "Cible" :         #Omissions 
            BR=0
            R_Type="Omission"
            O=1
            
            Hit=0
            Rejet=0
            FR=0
            
        elif key[0] == oui and stimType == "Distracteur" :       #Fausses Reconnaissances
            BR = 0
            R_Type="FR"
            FR=1
            
            Hit=0
            Rejet=0
            O=0
        elif key[0] =="escape":                 #possibilité de quitter l’expérience avec la touche echap
            datafile.close()
            win.close()
            core.quit()
            
        else : 	
            BR=0
            R_Type="erreur paramétrage ou absence de rep clavier"
            Hit=0
            O=0
            Rejet=0
            FR=0
        
        #Incrémentation des compteurs de bonnes réponses 
        
        TR=responseTime-imageTime
        comptTR=comptTR+TR
        comptBR=comptBR+BR 
        comptHit=comptHit+Hit 
        comptRejet=comptRejet+Rejet
        comptO=comptO+O
        comptFR=comptFR+FR
        
        #Récupération des informations sur le stimulus à traiter en vu de son stockage 
        stimGroupe=numStim['IDGroupe']
        stimID=numStim['ID']
        stimPT_Med=numStim['PreTest_Mediane']
        stimPT_Moy=numStim['PreTest_Moyenne']
        stimPT_Fid=numStim['PreTest_Fidelite']
        stimPT_Rang=numStim['PreTest_Rang']
        
        
            ## repClavier = réponse clavier du participant, typeRep= type de réponse rejet correct, hit, FA, omission 
            ## BR= justesse de la réponse, TR = temps de réaction
            ## PT_... = indices des pré-tests sur les stimuli

        #Ecriture du fichier de sortie
        ##Remarque : enregistrement des TR selon une chaîne de 3 caractères
        
        
        writer.writerow([ID, Sexe, Age, verbalisationCondF, filtrageCondF, IDbloc, NumEssai, stimGroupe, stimType, stimID, key[0], R_Type, BR, "{:.3f}".format(responseTime-imageTime), stimPT_Med, stimPT_Moy, stimPT_Fid, stimPT_Rang]) 
        
        event.clearEvents()         #réinitialisation des événements clavier
        
        emptyScreen = visual.ImageStim(win, image =stim_path+"EmptyScreen.jpg")
        emptyScreen.draw()
        win.flip()
        core.wait(0.5)
        
        NumEssai=NumEssai+1 #incrémentation du numéro de l'essai

    #Fin du bloc 
    finBloc = visual.TextStim(win,color="white",height=40)
    finBloc.setText("Fin du bloc "+str(IDbloc)) #se référer au paramétrage de l'expérience pour le texte de cette consigne
    finBloc.draw()
    win.flip()
    core.wait(1)
    
    NumEssai= 1 #réinitialisation du numéro de l'essai à chaque bloc
    NumBloc=NumBloc+1 #incrémentation du numéro du bloc

#Fermeture du fichier de sortie des données individuelles participants
datafile.close()

# Calcul des scores globaux exprimés en % 
ScoreHit=(comptHit/10)*100
ScoreRejet=(comptRejet/90)*100
scoreO=(comptO/10)*100
ScoreFR=(comptFR/90)*100
ScoreBR=(comptBR/100)*100
ScoreTR=comptTR/100

# Ouverture d'un classeur contenant les données résumées de tous les participants pour lecture et ajout des données relatives à la passation
dataframeR = open(data_path+"MatriceG/"+"dataFrameR.csv", "a") 
writer_DFR = csv.writer(dataframeR, delimiter=";")
writer_DFR.writerow([ID, Experimentateur, Sexe, Age, verbalisationCondF, filtrageCondF, comptBR, ScoreBR, comptHit, ScoreHit, comptFR, ScoreFR, comptO, scoreO, comptRejet, ScoreRejet, comptTR, ScoreTR]) 
dataframeR.close() ##Fermeture

    ## ID = identifiant du participant, Experimentateur = nom de l'experimentateur 
    ## Sexe, Age = stockage du sexe et de l'age du participant 
    ## VI : condition Verbalisation et condition Filtrage du participant 
    ## comptBR, comptHit, comptFR, comptO, comptRejet = valeurs brutes résumées des scores associés à ces différents types de réponses (BR = bonne réponse)
    ## ScoreBR, ScoreHit, ScoreFR, ScoreO, ScoreRejet = valeurs exprimées en % des scores associés à ca différents types de réponses 


## Bip de timing pour l'expérimentateur 
highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
highA.setVolume(0.8)
highA.play()
core.wait(0.8)

#Message de fin d'expérience

messageFin = visual.TextStim(win, alignHoriz='center', color="white",height=30)
messageFin.setText("Fin de la tâche. Suivez les instructions de l'expérimentateur pour les tâches qui vont suivre.")
messageFin.draw()
win.flip()
event.waitKeys()

event.clearEvents()

#Test des formes identiques de Thurstone
consigne = visual.TextStim(win,
                           text= 'Tâche de Thurstone : Appuyez sur la barre d\'espace lorsque vous êtes prêt·e à commencer la tâche', 
                           color="white", 
                           alignHoriz='center',
                           alignVert='center',
                           height=30)
consigne.draw()
win.flip()
event.waitKeys()
event.clearEvents()

Thurstone = visual.TextStim(win, alignHoriz='center', color="white",height=40)
Thurstone.setText("Durée : 4 minutes")
Thurstone.draw()
win.flip()
#core.wait(5)
core.wait(240)

## Bip de timing pour l'expérimentateur 
highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
highA.setVolume(0.8)
highA.play()
core.wait(0.8)

# Message de fin d'expérience
messageFin = visual.TextStim(win, alignHoriz='center', color="white",height=30)
messageFin.setText("Fin de l'expérience, merci beaucoup pour votre participation !")
messageFin.draw()
win.flip()
core.wait(5)

#Fermeture des fichiers d’entrée et de sortie et de la fenêtre d’affichage

win.close()
core.quit()
