# -*- coding: utf-8 -*-
import csv

# Dataframe contenant la liste des noms de fichiers individuels à ouvrir 
nomFichiers=open("NomSujets.csv","r")
db_nomFichiers=csv.reader(nomFichiers,delimiter=";")
db_keyFichiers=list(db_nomFichiers)
#print(db_keyFichiers[1][0])
sizeKeyFichiers=len(db_keyFichiers)

data_path = './data/'

#Code alternatif pour charger la liste d'items à partir d'un classeur excel 
    #nomFichiers_Liste=open("ListeItems.csv","r")
    #db_nomFichiers_Liste=csv.reader(nomFichiers_Liste,delimiter=";")
    #db_ListeFichiers=list(db_nomFichiers_Liste)
    #il vous faut ensuite stocker dans liste_items, une liste de Strings correspondant aux noms des items, si tel n'est pas déjà le cas (i.e., si vous avez importé un dataframe qui contient d'autres variables)

# Entete de nos nouveaux fichiers 
liste_items=["F003Chicago_F021","F003Chicago_F036","F003Chicago_F211","F003Chicago_F219","F003Chicago_F029","F003Chicago_F031","F003Chicago_F003","F003Chicago_F221","F003Chicago_F035","F003Chicago_F009","F02KDE_F04","F02KDE_F12","F02KDE_F24","F02KDE_F08","F02KDE_F11","F02KDE_F09","F02KDE_F29","F02KDE_F02","F02KDE_F34","F02KDE_F13","F091London_F013","F091London_F097","F091London_F124","F091London_F001","F091London_F086","F091London_F091","F091London_F011","F091London_F007","F091London_F144","F091London_F039","F107Chicago_F015","F107Chicago_F245","F107Chicago_F220","F107Chicago_F212","F107Chicago_F205","F107Chicago_F012","F107Chicago_F238","F107Chicago_F206","F107Chicago_F215","F107Chicago_F107","F248Chicago_F037","F248Chicago_F222","F248Chicago_F217","F248Chicago_F022","F248Chicago_F247","F248Chicago_F249","F248Chicago_F248","F248Chicago_F230","F248Chicago_F232","F248Chicago_F203","H013KDE_H021","H013KDE_H018","H013KDE_H014","H013KDE_H025","H013KDE_H008","H013KDE_H013","H013KDE_H033","H013KDE_H005","H013KDE_H022","H013KDE_H024","H117London_H017","H117London_H018","H117London_H117","H117London_H012","H117London_H103","H117London_H026","H117London_H004","H117London_H033","H117London_H063","H117London_H115","H203Chicago_H240","H203Chicago_H229","H203Chicago_H004","H203Chicago_H016","H203Chicago_H203","H203Chicago_H254","H203Chicago_H213","H203Chicago_H209","H203Chicago_H208","H203Chicago_H242","H245Chicago_H037","H245Chicago_H234","H245Chicago_H245","H245Chicago_H010","H245Chicago_H235","H245Chicago_H006","H245Chicago_H214","H245Chicago_H217","H245Chicago_H011","H245Chicago_H232","H249Chicago_H206","H249Chicago_H225","H249Chicago_H216","H249Chicago_H248","H249Chicago_H205","H249Chicago_H249","H249Chicago_H221","H249Chicago_H258","H249Chicago_H204","H249Chicago_H020"]
BR=['ok']*(len(liste_items)+1)
TR=['ok']*(len(liste_items)+1)

nameRows=[]
nameRows.append("ID")
for i in range(len(liste_items)):
    nameRows.append(liste_items[i])


#On créé 2 fichiers distincts pour les BR et les TR
## Eventuellement, envisager de créer 2 feuilles serait plus propre 
## Pour chacun, création d'une ligne d'entête avec les intitulés contenus dans nameRows
fileName2="BR"
ItemWrite=open(fileName2+".csv", "wb")
writeFile2=csv.writer(ItemWrite,delimiter=";")
writeFile2.writerow(nameRows)

fileName3="TR"
ItemWrite2=open(fileName3+".csv", "wb")
writeFile3=csv.writer(ItemWrite2,delimiter=";")
writeFile3.writerow(nameRows)


# Remplissage des deux fichiers TR et BR 

nom=0 ##Initialisation de la boucle 

while nom < sizeKeyFichiers:
    
    nameS=db_keyFichiers[nom][0]
    #print(nameS)
    
    datafile=open(data_path+nameS+".csv","r") #ouverture de chaque fichier individuel grâce à la liste definie dans db_keyFichiers 
    dbCSV=csv.reader(datafile,delimiter=";")
    db=list(dbCSV)
    del db[0]#remove the first row

    dbSize=len(db)-1
    
    #Conversion des matrices individuelles en dico pour faciliter la prise d'infos
    dico_db = {}

    keys=db[0]
    
    nb_ligne = [1] * len(db)
    for i in range(len(keys)):
        nom_key=keys[i]
        for j in range(len(nb_ligne)):
            dico_db[nom_key,j]=db[j][i]
    
    
    BR[0]=dico_db["ID",1]
    TR[0]=dico_db["ID",1]
    
    # Remplissage des matrices TR et BR
    for i in range(len(liste_items)):
        
        j=1
        ex=liste_items[i]
        
        while j< dbSize:
            ex_dico=dico_db["idStim",j]
            
            if (ex==ex_dico):
                coordinate_BR=j
                BR[i+1]=dico_db["BR",coordinate_BR]
                TR[i+1]=dico_db["TR",coordinate_BR]
    
        
            j+=1   
         
    writeFile2.writerow(BR)
    writeFile3.writerow(TR)

    datafile.close() #fermeture du fichier individuel
    
    #Réinitialisation des dataframes et dictionnaire individuels  
    del dico_db 
    del dbCSV
    del db
    
    nom+=1




