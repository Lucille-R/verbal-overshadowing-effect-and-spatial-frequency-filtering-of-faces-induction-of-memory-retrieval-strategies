library(dplyr) #library from tidyverse, useful for data manipulation

setwd("~/Documents/Master SCO/Semestre 2/Nettoyage_data") #set directory
#getwd() #check directory

name.sujets<-read.csv("NomSujets.csv",sep=";",header=FALSE) #read a csv file with a list of individual data filenames 

#Initialization of a dataframe with 11 col
data.New <- data.frame(matrix(ncol = 11, nrow = 0), stringsAsFactors=FALSE) 
colnames(data.New)<-c("ID","Sexe","Age","CondVerbalisation","CondFiltrage","BR_P","Hit_P","Omissions_P","Rejets_P","FR_P","MeanTR")

# Initialisation of the loop#1
nSujet=1

while(nSujet<=120){
  
  ## LOADING & CLEANING FILES
  name<-paste("./data/",name.sujets$V1[nSujet],".csv",sep="") #define the name of the file to read 
  print(name) #check 
  
  
  setwd("~/Documents/Master SCO/Semestre 2/Nettoyage_data")
  
  sujet1<-read.csv(name,header=FALSE,sep=";",dec=".",na.strings="NA") #read the nSujet th csv file
  sujet1<-sujet1[-1,] #remove the first row
  sujet1<-sujet1[-1,] #remove the 2nd row 
  
  vector <- c("ID",
              "Sexe",
              "Age",
              "CondVerbalisation",
              "CondFiltrage",
              "bloc",
              "essai",
              "liste",
              "type",
              "idStim",
              "repClavier",
              "typeRep",
              "BR",
              "TR",
              "PT_Med",
              "PT_Moyenne",
              "PT_Fidelite","PT_Rang")
  colnames(sujet1)<-vector #header of sujet1 file 
  
  # Convert BR et TR in num
  #sujet1[,13] <-as.numeric(paste(sujet1[,13]))
  #sujet1[,14] <-as.numeric(paste(sujet1[,14]))
  sujet1$BR<- as.numeric(paste(sujet1$BR),na.rm=TRUE)
  sujet1$TR<- as.numeric(paste(sujet1$TR),na.rm=TRUE)
  #print(str(sujet1)) #check 
  print(sujet1$ID[1])
  
  ## COUNTING Hits, FA, Rejects, Omissions
  
  # Count on the original file 
  count.Rep<-sujet1 %>% group_by(typeRep) %>% summarize(eff=n()) %>% arrange(typeRep)
  #print(count.Rep) #Check 
  if(length(count.Rep$typeRep)!=4){
    if('FR' %in% count.Rep$typeRep ==FALSE){
      replace<-data.frame(typeRep="FR",eff=0)
      }
    else if('Hit' %in% count.Rep$typeRep ==FALSE){
      replace<-data.frame(typeRep="Hit",eff=0)
      }
    else if('Rejet' %in% count.Rep.f$typeRep ==FALSE){
      replace<-data.frame(typeRep="Rejet",eff=0)
      }
    else if('Omission' %in% count.Rep$typeRep ==FALSE){
      replace<-data.frame(typeRep="Omission",eff=0)
      }
    count.Rep<-rbind(count.Rep,replace)
  }
  count.Rep <- count.Rep %>% arrange(typeRep)
  
  # Filter TR between 200 ms & 4.5 s
  filter.sujet1 <- sujet1 %>% filter(TR > 0.200 & TR<4.5)
  #print(filter.sujet1) #Check
  
  # Count on the file (filtered version)
  count.Rep.f <- filter.sujet1 %>% group_by(typeRep) %>% summarize(eff=n()) %>% arrange(typeRep)
  #print(count.Rep.f) #Check
  
  if(length(count.Rep.f$typeRep)!=4){
    if('FR' %in% count.Rep.f$typeRep ==FALSE){
      replace<-data.frame(typeRep="FR",eff=0)
      }
    else if('Hit' %in% count.Rep.f$typeRep ==FALSE){
      replace<-data.frame(typeRep="Hit",eff=0)
      }
    else if('Rejet' %in% count.Rep.f$typeRep ==FALSE){
      replace<-data.frame(typeRep="Rejet",eff=0)
      }
    else if('Omission' %in% count.Rep.f$typeRep ==FALSE){
      replace<-data.frame(typeRep="Omission",eff=0)
    }
    count.Rep.f<-rbind(count.Rep.f,replace)
  }
  count.Rep.f <- count.Rep.f %>% arrange(typeRep)
  
  
  # New loop#2 to compute differences 
  
  #Initialization
  i=1 
  nb=list()
  
  #Loop#2 
  while(i<5){
    
    nb[i]<- count.Rep[i,2]-count.Rep.f[i,2] #compute the difference between original & filtering effectives
    
    i=i+1
  } #end lopp#2
  
  
  vec<-paste(count.Rep.f$typeRep)
  Res<-as.data.frame(cbind(vec,nb)) #summarize the result from loop#2
  #print(Res)#Check 
  
  # Denominators to calculate proportions  
  denom.FR.Rej<-90-(as.numeric(paste(Res$nb[Res$vec=="FR"])) + as.numeric(paste(Res$nb[Res$vec=="Rejet"])))
  #print(denom.FR.Rej) #Check 
  
  denom.Hit.O<-10-(as.numeric(paste(Res$nb[Res$vec=="Hit"])) + as.numeric(paste(Res$nb[Res$vec=="Omission"])))
  print(paste("denominateur:",denom.Hit.O)) #Check
  
  
  # Calculate proportions for each type of response 
  
  count.Rep.f2<-count.Rep.f %>% mutate (prop = case_when( typeRep=="Hit" | typeRep=="Omission" ~ (eff*100/denom.Hit.O), 
                                                          typeRep=="FR" | typeRep=="Rejet" ~ (eff*100/denom.FR.Rej))
  )
  
  #print(count.Rep.f2) #Check
  
  # Proportion of BR 
  
  BR.P=count.Rep.f$eff[count.Rep.f$typeRep=="Hit"]+count.Rep.f$eff[count.Rep.f$typeRep=="Rejet"]
  #print(BR.P) #Check 
  
  MeanTR<-mean(sujet1$TR,na.rm=TRUE) #Mean TR 
  #print(MeanTR)
  
  ## FILL DATAFRAME TO STORE INDIVIDUAL DATA 
  
 data.Inc<-data.frame(ID=paste(sujet1$ID[1]),
                      Sexe=paste(sujet1$Sexe[1]),
                      Age=paste(sujet1$Age[1]),
                      CondVerbalisation=paste(sujet1$CondVerbalisation[1]),
                      CondFiltrage=paste(sujet1$CondFiltrage[1]),
                      BR_P=BR.P,
                      Hit_P=count.Rep.f2$prop[count.Rep.f2$typeRep=="Hit"],
                      Omissions_P=count.Rep.f2$prop[count.Rep.f2$typeRep=="Omission"],
                      Rejets_P=count.Rep.f2$prop[count.Rep.f2$typeRep=="Rejet"],
                      FR_P=count.Rep.f2$prop[count.Rep.f2$typeRep=="FR"],
                     MeanTR=MeanTR)
  print(data.Inc) #Check 

  #Incrementation
  data.New<-rbind(data.New,data.Inc)
  #print(data.New) #Check 
  
  nSujet=nSujet+1 #increment loop#1
  
} #end of the loop#1

#head(data.New)

## ADD d' et C from TDS in Data.New 
data.New<-data.New %>% mutate(d = case_when(Hit_P!=100 & FR_P !=0 ~ qnorm(Hit_P/100)-qnorm(FR_P/100),
                                            Hit_P==100 & FR_P !=0 ~ qnorm(10.5/11)-qnorm(FR_P/100),
                                            Hit_P!=100 & FR_P ==0 ~ qnorm(Hit_P/100)-qnorm(0.5/91)),
                              
                              C = case_when(Hit_P!=100 & FR_P !=0 ~ (-0.5)*(qnorm(Hit_P/100)+qnorm(FR_P/100)),
                                            Hit_P==100 & FR_P !=0 ~ (-0.5)*(qnorm(10.5/11)+qnorm(FR_P/100)),
                                            Hit_P!=100 & FR_P ==0 ~ (-0.5)*(qnorm(Hit_P/100)+qnorm(0.5/91)))
                              )
#head(data.New) #Check 

## LOAD & EXTRACT individual informations (e.g. from psychometric tools) from our original dataframe
setwd("~/Documents/Master SCO/Semestre 2/Rapport")
#getwd() #Check
data<-read.csv("Data3.csv",header=T,sep=";",dec=",",na.strings="NA")

#ID & scores extraction 
list.toBind<-c(1:2,21:44) 
data.ToBind<-data[,list.toBind]
#head(data.ToBind) #Check 

# Merging the two dataframes by ID  
data.Clean<-merge(data.New,data.ToBind,by="ID")
#head(data.Clean) #Check 

## SAVE CLEAN DATAFRAMES in csv files 
write.table(data.New, "MatriceNettoyee.csv", sep=";",dec=".", na="NA",row.names = FALSE)
write.table(data.Clean, "MatriceNettoyee_full.csv", sep=";",dec=".", na="NA",row.names = FALSE)
